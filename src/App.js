import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import Chat from './bsa';
import PrivateRoute from './components/Routes/PrivateRoute';
import PublicRoute from './components/Routes/PublicRoute';
import Auth from './components/Auth/Auth.jsx';
import Users from './components/Users/Users.jsx';
import UserEdit from './components/UserEdit/UserEdit.jsx';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Routes>
      <Route exact path="/login" element={<Auth />} />
      <Route exact path='/' restricted element={<Chat className="chat" url="https://example.com" />}/>
      <Route exact path='/users' element={<PrivateRoute component={<Users/>} />} />
      <Route exact path='/users/edit/:id' element={<PrivateRoute component={<UserEdit />} />} />
      </Routes>
      </ BrowserRouter>
    </div>
  );
}

export default App;