import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import authSelectors from '../../redux/auth/auth-selectors';

export default function PrivateRoute({ children, ...routeProps }) {
  const isLoggedIn = useSelector(authSelectors.getIsLoggedIn);
  // return (
    // <Route {...routeProps}>
      // eslint-disable-next-line no-lone-blocks
      {/* {isLoggedIn ? { element = children } : <Navigate to="/auth" replace={true} />} */}
      return <>{isLoggedIn ? children : <Navigate to="/auth" replace={true} />} </>;
    // </Route>
  // );
}