import { useSelector } from 'react-redux';
import { Route, Navigate } from 'react-router-dom';
import authSelectors from '../../redux/auth/auth-selectors';

export default function PublicRoute({
  children,
  restricted = false,
  ...routeProps
}) {
  const isLoggedIn = useSelector(authSelectors.getIsLoggedIn);
  console.log(isLoggedIn);
  const shouldRedirect = isLoggedIn && restricted;
//   return (
//     <Route {...routeProps}>
//       {shouldRedirect ? <Navigate to="/" replace={true} /> : children}
      return <>{shouldRedirect ? <Navigate to="/" replace={true} /> : children} </>;
    // </Route>
//   );
}