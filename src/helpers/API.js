import axios from 'axios';
axios.defaults.baseURL = 'https://bsa-chat.azurewebsites.net/';

const setToken = {
  set(token) {
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
  },
  unset() {
    axios.defaults.headers.common.Authorization = '';
  },
};

const getUsers = () => {
  return axios.get('/api/Users').then(data => data);
};

const registerUser = async ({ name, email, password, avatar }) => {
  const { data } = await axios.post('/api/Users', { name, email, password, avatar });
  return data;
};

const getUser = (id) => {
  return axios.get('/api/Users/${id}').then(data => data);
};

const deleteUser = async (id) => {
  const { data } = await axios.delete('/api/Users', { id });
  return data;
};

export {
  getUsers,
  registerUser,
  setToken,
  getUser,
  deleteUser
};