
import { combineReducers } from '@reduxjs/toolkit';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import reducerLC from './LC/LC-Reducer';
import authReducer from '../redux/auth/auth-reducer';

const contactPersistConfig = {
  key: 'token',
  storage: storage,
};

export const rootReducer = combineReducers({
  localSt: persistReducer(contactPersistConfig, reducerLC),
  auth: authReducer,
});

export default rootReducer;